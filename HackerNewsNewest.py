from telegram.ext import Updater
from telegram import ParseMode
from concurrent import futures
import requests
import telegram

BASE_URL = "https://hacker-news.firebaseio.com/v0/{method}.json"

def escape_markdown(text):
    SPECIAL_CHARS = ['\\', '_', '*', '[', ']', '(', ')', '~', '`', '>', '<', '&', '#', '+', '-', '=', '|', '{', '}', '.', '!']
    for char in SPECIAL_CHARS:
        text = text.replace(char,'\\'+char)
    return text

def call_method(method):
    result = requests.get(BASE_URL.format(method=method))
    if result.ok:
        return result.json()
    return None

def get_stories(stories_type):
    return call_method(stories_type)

def get_item(id):
    return call_method("item/{}".format(id))

def save_last_item(item):
    with open("lastitem", "w") as text_file:
        text_file.write(item)

def load_last_item():
    with open("lastitem", "r") as text_file:
        previous_item = text_file.read()
    return int(previous_item)

def is_new(old_time, new_time):
    return int(new_time) > int(old_time)

def get_article_url(item):
    try:
        return item["url"]
    except:
        return ""

def send_message(item):
    message = item["title"] + "\n[Article](" + get_article_url(item) +"), [Comments](" + "https://news.ycombinator.com/item?id={}".format(item["id"]) + ")"
    bot.send_message(chat_id=chat_id, text=escape_markdown(message), parse_mode=ParseMode.MarkdownV2)

def handle_story(story):
    item = get_item(story)
    print(story)
    send_message(item)

def send_newstories(job_queue):
    newstories = get_stories("newstories")
    try:
        real_newstories = newstories[newstories.index(load_last_item())::-1]
        if len(real_newstories) == 1:
            return
    except:
        save_last_item(str(newstories[5]))
        return
    with futures.ProcessPoolExecutor() as pool:
        for story in real_newstories[:-1]:
            future_result = pool.submit(handle_story, story)
    save_last_item(str(newstories[0]))

chat_id = "@HackerNewsNewest"
TOKEN = open('token').read().strip('\n')
bot = telegram.Bot(token=TOKEN)
updater = Updater(token=TOKEN)
job_queue = updater.job_queue
job_queue.run_repeating(callback=send_newstories, interval=60)

updater.start_polling()
