addEventListener('fetch', event => {
  event.respondWith(eventHandler(event));
});

addEventListener('scheduled', event => {
  event.waitUntil(handleScheduled(event));
});

async function call_method(method) {
	let result = await fetch(`https://hacker-news.firebaseio.com/v0/${method}.json`);
	let data = await result.json();
	return data;
}

async function get_stories(stories_type) {
	return call_method(stories_type)
}

async function save_last_item(item) {
	await items.put("lastitem", item);
}

async function get_item(id) {
	return call_method(`item/${id}`);
}

async function get_article_url(item) {
	try {
		return item.url;
	}
	catch {
		return "";
	}
}

async function send_message(item) {
	if(!item.title) {
		throw "undefined";
	}
	post_url = await get_article_url(item);
	title = encodeURIComponent(item.title);
	message = `${title}\n[Article](${post_url}), [Comments](https://news.ycombinator.com/item?id=${item.id})`;
	const url = `https://api.telegram.org/bot` + API_KEY + `/sendMessage?parse_mode=MarkdownV2&chat_id=` + chat_id + `&text=${message}`;
	const data = await fetch(url).then(resp => resp.json());
}

async function handle_story(story) {
    item = await get_item(story);
    await send_message(item);
}

async function logic() {
	newstories = await get_stories("newstories");
	try {
		previous_item = Number(await items.get("lastitem"));
		real_newstories = newstories.slice(0,newstories.indexOf(previous_item)).reverse();
		if(0 == real_newstories.length) {
			return new Response("OK");
		}
		if(499 == real_newstories.length) {
			throw "Needs reset";
		}
		for await (const story of real_newstories) {
			await handle_story(story);
		}
		await save_last_item(newstories[0].toString());
		return new Response("OK");
	}
	catch (error) {
		console.error(error);
		await save_last_item(newstories[5].toString());
		return new Response(error);
	}
}

async function eventHandler(event) {
  return await logic();
}

async function handleScheduled(event) {
  return await logic();
}
